This is a plugin for Atlassian products that provides support for Suppliers. Suppliers provide
access to data and other values within an application in a simple but flexible manner.

For more details about this plugin, visit the wiki:

https://bitbucket.org/randomeizer/supplier-core/wiki
