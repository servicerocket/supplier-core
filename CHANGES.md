Supplier Core
=============

2016-07-04, 1.4.1
-----------------
 * WIKI-734 Compatible with Confluence Data Center.
 
2016-06-13, 1.4.0
-----------------
 * Compatible with Confluence 5.10.0.
 
2016-02-16, 1.3.2
-----------------
 * REPORT-695 Fix issue with wrong week of year being rendered in `{date-supplier}`.
 
2015-12-23, 1.3.1
-----------------
 * Change the modifier of addSupplier and removeSupplier method to public.
 
2015-12-1, 1.3.0
-----------------
 * Compatible with Confluence 5.9.1.
 
2015-11-16, 1.2.1
-----------------
 * Add missing Map Supplier to plugin descriptor

2015-10-28, 1.2.0
-----------------
 * WIKI-536 Remove duplicated user supplier.

2015-10-06, 1.1.0
-----------------
 * WIKI-488 Compatible with Confluence 5.9.

2014-12-16, 1.0.10
-----------------
 * WIKI-126 Change the return type of TextSupplier on method splitWith().