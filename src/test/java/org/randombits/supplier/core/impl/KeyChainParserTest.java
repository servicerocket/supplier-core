package org.randombits.supplier.core.impl;

import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.internal.ExpectationBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.randombits.supplier.core.SupplierException;

import static org.junit.Assert.*;

@RunWith(JMock.class)
public class KeyChainParserTest {

    private Mockery context = new JUnit4Mockery();

    private <T> T mock( Class<T> type ) {
        return context.mock( type );
    }

    private void checking( ExpectationBuilder expectations ) {
        context.checking( expectations );
    }

    private void assertKeys( KeyChain keyChain, Key... keys ) {
        int i = 0;
        for ( i = 0; i < keys.length; i++ ) {
            assertTrue( keyChain.hasNext() );
            assertEquals( keys[i], keyChain.next() );
        }
        assertFalse( "KeyChain has extra keys than expected.", keyChain.hasNext() );
    }

    @Test
    public void testParseOne() throws SupplierException {
        KeyChain keys = KeyChainParser.parse( "one" );
        assertNotNull( keys );
        assertKeys( keys, new Key( "one" ) );
    }

    @Test
    public void testParsePrefixed() throws SupplierException {
        KeyChain keys = KeyChainParser.parse( "prefixed:one" );
        assertNotNull( keys );
        assertKeys( keys, new Key( "prefixed", "one" ) );
    }

    @Test
    public void testParseSimpleChain() throws SupplierException {
        KeyChain keys = KeyChainParser.parse( "one > two" );
        assertNotNull( keys );
        assertKeys( keys, new Key( "one" ), new Key( "two" ) );
    }

    @Test
    public void testParseQuoted() throws SupplierException {
        KeyChain keys = KeyChainParser.parse( "\"one > two\"" );
        assertNotNull( keys );
        assertKeys( keys, new Key( "one > two" ) );
    }

    @Test
    public void testParseQuotedChain() throws SupplierException {
        KeyChain keys = KeyChainParser.parse( "\"one \\\"quoted\\\"\" > \"two quoted\"" );
        assertNotNull( keys );
        assertKeys( keys, new Key( "one \"quoted\"" ), new Key( "two quoted" ) );
    }

    @Test
    public void testParseQuotedIncomplete() {
        try {
            KeyChainParser.parse( "\"one" );
        } catch ( SupplierException e ) {
            // Expected result
            return;
        }
        fail( "Expected a SupplierException" );
    }

    @Test
    public void testParseQuotedEscapeBroken() {
        try {
            KeyChainParser.parse( "\"one\\\"" );
        } catch ( SupplierException e ) {
            // Expected result
            return;
        }
        fail( "Expected a ParameterParsingException" );
    }

}
