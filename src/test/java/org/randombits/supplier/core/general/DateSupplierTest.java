package org.randombits.supplier.core.general;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.mockito.runners.MockitoJUnitRunner;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author williamtanweichun
 * @since 1.3.2.20160216
 */
@RunWith(MockitoJUnitRunner.class)
public class DateSupplierTest {
    DateSupplier $;
    @Mock SupplierContext supplierContext;
    @Before public void setUp() throws Exception {
        $ = new DateSupplier();
    }

    @Test
    public void shouldReturnFormattedString() throws SupplierException, ParseException {

        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date dec312020 = sdf.parse("2020-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312020);
        assertEquals("2020-12-31 should return 53 as the number of week of the year 2020", "53", $.format(supplierContext, "w"));

        Date dec312019 = sdf.parse("2019-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312019);
        assertEquals("2019-12-31 should return 1 as the number of week of the year 2019", "1", $.format(supplierContext, "w"));

        Date dec312018 = sdf.parse("2018-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312018);
        assertEquals("2018-12-31 should return 1 as the number of week of the year 2018", "1", $.format(supplierContext, "w"));

        Date dec312017 = sdf.parse("2017-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312017);
        assertEquals("2017-12-31 should return 1 as the number of week of the year 2017", "1", $.format(supplierContext, "w"));

        Date dec312016 = sdf.parse("2016-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312016);
        assertEquals("2016-12-31 should return 52 as the number of week of the year 2016", "52", $.format(supplierContext, "w"));

        Date dec312015 = sdf.parse("2015-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312015);
        assertEquals("2015-12-31 should return 52 as the number of week of the year 2015", "52", $.format(supplierContext, "w"));

        Date dec312014 = sdf.parse("2014-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312014);
        assertEquals("2014-12-31 should return 53 as the number of week of the year 2014", "53", $.format(supplierContext, "w"));

        Date dec312013 = sdf.parse("2013-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312013);
        assertEquals("2013-12-31 should return 1 as the number of week of the year 2013", "1", $.format(supplierContext, "w"));

        Date dec312012 = sdf.parse("2012-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312012);
        assertEquals("2012-12-31 should return 1 as the number of week of the year 2012", "1", $.format(supplierContext, "w"));

        Date dec312011 = sdf.parse("2011-12-31");
        when(supplierContext.getValueAs(Date.class)).thenReturn(dec312011);
        assertEquals("2011-12-31 should return 52 as the number of week of the year 2011", "52", $.format(supplierContext, "w"));
    }

    @Test(expected = SupplierException.class)
    public void shouldThrowSupplierException() throws SupplierException {
        when(supplierContext.getValueAs(Date.class)).thenReturn(new Date());
        $.format(supplierContext, "wrongformat");
    }
}