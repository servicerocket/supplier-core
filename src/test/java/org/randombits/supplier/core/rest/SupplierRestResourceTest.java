package org.randombits.supplier.core.rest;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.internal.ExpectationBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.randombits.supplier.core.SupplierAssistant;
import org.randombits.supplier.core.SupplierException;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

@RunWith(JMock.class)
public class SupplierRestResourceTest {

    private Mockery context = new JUnit4Mockery();

    private SupplierAssistant supplierAssistant;

    private <T> T mock( Class<T> type ) {
        return context.mock( type );
    }

    private void checking( ExpectationBuilder expectations ) {
        context.checking( expectations );
    }

    @Before
    public void setup() {
        supplierAssistant = mock( SupplierAssistant.class );
    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() throws SupplierException {
        final SupplierRestResource resource = new SupplierRestResource( supplierAssistant );

        final String keychain = "test:value";

        checking( new Expectations() {
            {
                one( supplierAssistant ).findValue( "", keychain );
                will( returnValue( null ) );
            }
        } );


        Response response = resource.getValue( keychain );
        final Object result = response.getEntity();

        assertNull( "expected null", result );
    }
}
