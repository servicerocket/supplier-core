package org.randombits.supplier.core.servlet;

import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

@SupplierPrefix("session")
@SupportedTypes(HttpSession.class)
@API("1.0.0")
public class SessionSupplier extends AnnotatedSupplier {

    @SupplierKey({"creation time", "creation date"})
    @API("1.0.0")
    public Date getCreationTime(@KeyValue HttpSession session) {
        return new Date(session.getCreationTime());
    }

    @SupplierKey("id")
    @API("1.0.0")
    public String getSessionId(@KeyValue HttpSession session) {
        return session.getId();
    }

    @SupplierKey("attributes")
    @API("1.0.0")
    public Map<String, Object> getAttributes(@KeyValue HttpSession session) {
        Map<String, Object> attrs = new java.util.HashMap<String, Object>();
        @SuppressWarnings("unchecked")
        Enumeration<String> keys = session.getAttributeNames();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            attrs.put(key, session.getAttribute(key));
        }
        return attrs;
    }
}
