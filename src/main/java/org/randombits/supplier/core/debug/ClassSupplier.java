package org.randombits.supplier.core.debug;

import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

@SupplierPrefix("class")
@SupportedTypes(Class.class)
public class ClassSupplier extends AnnotatedSupplier {

    @SupplierKey("package")
    @API("1.0.0")
    public Object getPackage(@KeyValue Class<?> type) {
        if (type.getPackage() != null)
            return type.getPackage().getName();
        else
            return null;
    }

    @SupplierKey("interfaces")
    @API("1.0.0")
    public Class<?>[] getInterfaces(@KeyValue Class<?> type) {
        return type.getInterfaces();
    }

    @SupplierKey("superclass")
    @API("1.0.0")
    public Class<?> getSuperclass(@KeyValue Class<?> type) {
        return type.getSuperclass();
    }

    @SupplierKey("simple name")
    @API("1.0.0")
    public String getSimpleName(@KeyValue Class<?> type) {
        return type.getSimpleName();
    }

    @SupplierKey("is member class")
    @API("1.0.0")
    public boolean isMemberClass(@KeyValue Class<?> type) {
        return type.isMemberClass();
    }

    @SupplierKey("is local class")
    @API("1.0.0")
    public boolean isLocalClass(@KeyValue Class<?> type) {
        return type.isLocalClass();
    }

    @SupplierKey("is anonymous class")
    @API("1.0.0")
    public boolean isAnonymousClass(@KeyValue Class<?> type) {
        return type.isAnonymousClass();
    }

    @SupplierKey("component type")
    @API("1.0.0")
    public Class<?> getComponentType(@KeyValue Class<?> type) {
        return type.getComponentType();
    }

    @SupplierKey("is array")
    @API("1.0.0")
    public boolean isArray(@KeyValue Class<?> type) {
        return type.isArray();
    }

    @SupplierKey("is enum")
    @API("1.0.0")
    public boolean isEnum(@KeyValue Class<?> type) {
        return type.isEnum();
    }

    @SupplierKey("is interface")
    @API("1.0.0")
    public boolean isInterface(@KeyValue Class<?> type) {
        return type.isInterface();
    }

    @SupplierKey("is primitive")
    @API("1.0.0")
    public boolean isPrimitive(@KeyValue Class<?> type) {
        return type.isPrimitive();
    }

    @SupplierKey("name")
    @API("1.0.0")
    public String getName(@KeyValue Class<?> type) {
        return type.getName();
    }
}
