package org.randombits.supplier.core.stats;

import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

@SupplierPrefix("stats")
@SupportedTypes(Stats.class)
@API("1.0.0")
public class StatsSupplier extends AnnotatedSupplier {

    @SupplierKey("sum")
    @KeyWeight(100)
    @API("1.0.0")
    public double getSum(@KeyValue Stats stats) {
        return stats.getSum();
    }

    @SupplierKey("value count")
    @KeyWeight(50)
    @API("1.0.0")
    public int getValueCount(@KeyValue Stats stats) {
        return stats.getValueCount();
    }

    @SupplierKey("empty count")
    @API("1.0.0")
    public int getEmptyCount(@KeyValue Stats stats) {
        return stats.getEmptyCount();
    }

    @SupplierKey("item count")
    @API("1.0.0")
    public int getItemCount(@KeyValue Stats stats) {
        return stats.getItemCount();
    }

    @SupplierKey("value average")
    @API("1.0.0")
    public double getValueAverage(@KeyValue Stats stats) {
        return stats.getValueAverage();
    }

    @SupplierKey("item average")
    @API("1.0.0")
    public double getItemAverage(@KeyValue Stats stats) {
        return stats.getItemAverage();
    }

    @SupplierKey("min value")
    @API("1.0.0")
    public Object getMinValue(@KeyValue Stats stats) {
        return stats.getMinValue();
    }

    @SupplierKey("max value")
    @API("1.0.0")
    public Object getMaxValue(@KeyValue Stats stats) {
        return stats.getMaxValue();
    }

    @SupplierKey("min item")
    @API("1.0.0")
    public Object getMinItem(@KeyValue Stats stats) {
        return stats.getMinItem();
    }

    @SupplierKey("max item")
    @API("1.0.0")
    public Object getMaxItem(@KeyValue Stats stats) {
        return stats.getMaxItem();
    }
}
