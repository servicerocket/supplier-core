package org.randombits.supplier.core.annotate;

import java.lang.annotation.*;

/**
 * Marks a method as being a Supplier Key handler. This works in conjunction with classes that
 * extend the {@link AnnotatedSupplier}.
 *
 * @see AnnotatedSupplier
 * @see SupplierPrefix
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SupplierKey {

    /**
     * @return the key value(s) that will be matched.
     */
    String[] value();
}
