package org.randombits.supplier.core.annotate;

import java.lang.annotation.*;

/**
 * Marks a parameter as being the {@link org.randombits.supplier.core.SupplierContext}
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface KeyContext {
}
