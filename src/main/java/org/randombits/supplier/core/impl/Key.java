package org.randombits.supplier.core.impl;

/**
 * Represents a single "prefix:name" key value.
 */
class Key {

    private final String prefix;

    private final String name;

    public Key( String name ) {
        this( null, name );
    }

    public Key( String prefix, String name ) {
        this.name = name;
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;

        Key key = (Key) o;

        if ( !name.equals( key.name ) ) return false;
        if ( prefix != null ? !prefix.equals( key.prefix ) : key.prefix != null ) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = prefix != null ? prefix.hashCode() : 0;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return ( prefix != null ? prefix + ":" : "" ) + name;
    }
}
