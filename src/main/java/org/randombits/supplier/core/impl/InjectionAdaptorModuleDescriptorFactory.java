package org.randombits.supplier.core.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;

/**
 * A factory for {@link org.randombits.supplier.core.impl.InjectionAdaptorModuleDescriptor}s.
 */
public class InjectionAdaptorModuleDescriptorFactory extends SingleModuleDescriptorFactory<InjectionAdaptorModuleDescriptor> {

    private final ModuleFactory moduleFactory;

    public InjectionAdaptorModuleDescriptorFactory( HostContainer hostContainer, ModuleFactory moduleFactory ) {
        super( hostContainer, "injection-adaptor", InjectionAdaptorModuleDescriptor.class );
        this.moduleFactory = moduleFactory;
    }

    @Override
    public ModuleDescriptor getModuleDescriptor( String type ) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        return hasModuleDescriptor( type ) ? new InjectionAdaptorModuleDescriptor( moduleFactory ) : null;
    }
}