package org.randombits.supplier.core.impl;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.NotNull;
import org.dom4j.Element;
import org.randombits.supplier.core.param.Adaptor;

/**
 *
 */
public class InjectionAdaptorModuleDescriptor extends AbstractModuleDescriptor<Adaptor<?>> {

    private Adaptor<?> adaptor;

    public InjectionAdaptorModuleDescriptor( ModuleFactory moduleFactory ) {
        super( moduleFactory );
    }

    @Override
    public void init( @NotNull final Plugin plugin, @NotNull Element element ) throws PluginParseException {
        super.init( plugin, element );
        adaptor = null;
    }

    @Override
    public Adaptor<?> getModule() {
        if ( adaptor == null )
            adaptor = moduleFactory.createModule( moduleClassName, this );
        return adaptor;
    }
}
