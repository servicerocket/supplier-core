/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.core.impl;

import org.randombits.supplier.core.SupplierException;
import org.randombits.utils.text.TextBuffer;
import org.randombits.utils.text.TokenIterator;

import java.util.BitSet;
import java.util.List;

/**
 * This class helps parse supplier key values of the following format:
 * <p/>
 * "prefix:Quoted key" > anotherprefix:Another key
 *
 * @author David Peterson
 */
class KeyChainParser {

    private static final char QUOTE_CHAR = '"';

    private static final char ESCAPE_CHAR = '\\';

    private static final char GT_CHAR = '>';

    private static BitSet WHITESPACE = new BitSet();

    private static BitSet SEPARATOR = new BitSet();

    private static BitSet RESERVED = new BitSet();

    private static BitSet UNQUOTED = new BitSet();

    private static BitSet QUOTABLE = new BitSet();

    static {
        WHITESPACE.set( ' ' );
        WHITESPACE.set( '\t' );
        WHITESPACE.set( '\r' );
        WHITESPACE.set( '\n' );

        SEPARATOR.set( GT_CHAR );

        RESERVED.set( GT_CHAR );
        RESERVED.set( QUOTE_CHAR );

        UNQUOTED.set( Character.MIN_VALUE, Character.MAX_VALUE, true );
        UNQUOTED.andNot( SEPARATOR );
        UNQUOTED.andNot( RESERVED );

        QUOTABLE.set( Character.MIN_VALUE, Character.MAX_VALUE, true );
        QUOTABLE.clear( QUOTE_CHAR );
        QUOTABLE.clear( ESCAPE_CHAR );
    }

    public static KeyChain parse( String keyChain ) throws SupplierException {
        TokenIterator i = new TokenIterator( keyChain.trim() );
        List<String> list = parseChain( i );
        if ( !i.atEnd() )
            throw new SupplierException( "Unexpected values at end: '" + i.getSequence( TokenIterator.ALL_CHARS ) + "'" );

        return new KeyChain( list.iterator() );

    }

    private static List<String> parseChain( TokenIterator i ) throws SupplierException {
        boolean continuing = true;

        List<String> list = new java.util.ArrayList<String>();

        while ( continuing && !i.atEnd() ) {
            list.add( parseListItem( i ) );

            // Clear any extra whitespace...
            i.getSequence( WHITESPACE );
            continuing = i.getSequence( SEPARATOR, 1 ) != null;
        }

        return list;
    }

    private static String parseListItem( TokenIterator i ) throws SupplierException {
        TextBuffer buff = new TextBuffer();

        boolean done = false;

        while ( !done ) {
            if ( i.getToken( QUOTE_CHAR ) != null ) {
                readQuotedItem( i, buff );
            } else {
                CharSequence value = i.getSequence( UNQUOTED, 1 );
                if ( value != null )
                    buff.add( value );
                else
                    done = true;
            }
        }

        return buff.toString().trim();
    }


    private static void readQuotedItem( TokenIterator i, TextBuffer buff ) throws SupplierException {
        while ( !i.atEnd() && !i.matchToken( QUOTE_CHAR ) ) {
            buff.add( i.getSequence( QUOTABLE ) );
            if ( i.getToken( ESCAPE_CHAR ) != null ) {
                CharSequence escaped = i.getSequence( TokenIterator.ALL_CHARS, 1, 1 );
                if ( escaped == null )
                    throw new SupplierException( "Expected a character to escape after '" + ESCAPE_CHAR + "'" );

                buff.add( escaped );
            }
        }
        if ( i.getToken( QUOTE_CHAR ) == null )
            throw new SupplierException( "Expected '" + QUOTE_CHAR + "'" );
    }
}
