package org.randombits.supplier.core;

import org.randombits.utils.collection.ABitSet;
import org.randombits.utils.lang.API;
import org.randombits.utils.text.TokenIterator;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents a single key pattern, providing the ability to parse
 * and process a pattern like:
 * <p/>
 * <code>my token {param 1}, {param 2}</code>
 */
@API
public class KeyPattern {

    public enum Status {
        /**
         * The complete key matches the complete pattern.
         */
        COMPLETE,

        /**
         * The key matches part of the pattern but is incomplete.
         */
        PARTIAL,

        /**
         * The key matches the pattern, but there are excess characters in the key that were unused.
         */
        EXCESS,

        /**
         * The key does not match the pattern.
         */
        UNMATCHED
    }

    private static interface Processor {

        Processor getNext();

        /**
         * Processes the key, potentially storing values into the parameters.
         *
         * @param key    The key value being processed.
         * @param params The set of parameters.
         * @return the processing status.
         */
        Status process( TokenIterator key, Map<String, ? super String> params );

    }

    private static class Token implements Processor {

        private final String value;

        private final Parameter next;

        public Token( String value, Parameter next ) {
            this.value = value;
            this.next = next;
        }

        public String getValue() {
            return value;
        }

        public Parameter getNext() {
            return next;
        }

        @Override
        public Status process( TokenIterator key, Map<String, ? super String> params ) {
            if ( key.getToken( value, false, false ) == null ) {
                // incomplete
                if ( key.atEnd() )
                    return Status.PARTIAL;
                else
                    return Status.UNMATCHED;
            }

            if ( next != null ) {
                // Pass it on.
                return next.process( key, params );
            } else if ( key.atEnd() ) {
                // Success!
                return Status.COMPLETE;
            } else {
                // Still characters left
                return Status.EXCESS;
            }
        }

        @Override
        public String toString() {
            return value + ( next != null ? next : "" );
        }
    }

    private static class Parameter implements Processor {

        private final String name;

        private final Token next;

        public Parameter( String name, Token next ) {
            this.name = name;
            this.next = next;
        }

        public String getName() {
            return name;
        }

        public Token getNext() {
            return next;
        }

        @Override
        public Status process( TokenIterator key, Map<String, ? super String> params ) {
            CharSequence value;
            Status result;
            if ( next != null ) {
                value = key.getSequence( TokenIterator.ALL_CHARS, next.getValue() );
                if ( value == null ) {
                    // Failure
                    result = key.atEnd() ? Status.PARTIAL : Status.UNMATCHED;
                } else {
                    result = next.process( key, params );
                }
            } else {
                value = key.getSequence( TokenIterator.ALL_CHARS );
                if ( value == null ) {
                    // Failure
                    result = Status.UNMATCHED;
                } else {
                    result = Status.COMPLETE;
                }
            }

            // Add the parameter after so that only fully matched keys populate the params.
            if ( result == Status.COMPLETE && params != null )
                params.put( name, value.toString() );

            return result;
        }

        @Override
        public String toString() {
            return "{" + name + "}" + ( next != null ? next : "" );
        }
    }

    private final static ABitSet TOKEN_SET = new ABitSet();

    static {
        TOKEN_SET.or( TokenIterator.ALL_CHARS );
        TOKEN_SET.clear( "{}" );
    }

    private final String pattern;

    private final Processor processor;

    private final Set<String> parameters;

    private KeyPattern( String pattern, Processor processor ) {
        this.pattern = pattern;
        this.processor = processor;

        parameters = findParameters();
    }

    private Set<String> findParameters() {
        Set<String> params = new HashSet<String>( 10 );
        Processor current = processor;
        while ( current != null ) {
            if ( current instanceof Parameter )
                params.add( ( (Parameter) current ).getName() );
            current = current.getNext();
        }
        return Collections.unmodifiableSet( params );
    }

    /**
     * Returns the pattern string used by this key.
     *
     * @return The pattern string.
     */
    @API("1.0.0")
    public String getPattern() {
        return pattern;
    }

    /**
     * Returns the set of parameter names in this key.
     *
     * @return The parameter names.
     */
    @API("1.0.0")
    public Set<String> getParameters() {
        return parameters;
    }

    /**
     * Checks if the provided key value matches this key.
     *
     * @param keyValue The key value.
     * @return the {@link Status} of the match.
     */
    @API("1.0.0")
    public Status match( String keyValue ) {
        return process( keyValue, null );
    }

    /**
     * Attempts to process the provided key string with this key pattern.
     * If successful, the {@link Status#COMPLETE} value will be returned and
     * the <code>params</code> map will contain any named parameter values.
     * <p/>
     * <p>The <code>params</code> map may be null, in which case the method call
     * essentially becomes a 'match' check.</p>
     *
     * @param keyValue The key to try processing.
     * @param params   The map to put any parameters into. May be null.
     * @return the {@link Status} of the processing request.
     */
    @API("1.0.0")
    public Status process( String keyValue, Map<String, ? super String> params ) {
        TokenIterator keyIterator = new TokenIterator( keyValue );
        return processor.process( keyIterator, params );
    }

    @Override
    public String toString() {
        return "Key{" +
                "pattern='" + pattern + '\'' +
                '}';
    }

    /**
     * Parses the provided pattern and creates a KeyPattern instance to represent it.
     *
     * @param pattern The pattern to parse.
     * @return the {@link KeyPattern} instance.
     * @throws KeyParseException if there is a problem while parsing.
     */
    @API("1.0.0")
    public static KeyPattern parse( String pattern ) throws KeyParseException {
        Processor processor = parsePattern( pattern );
        return new KeyPattern( pattern, processor );
    }

    private static Processor parsePattern( String pattern ) throws KeyParseException {
        TokenIterator key = new TokenIterator( pattern );
        return parseToken( key );
    }

    private static Processor parseToken( TokenIterator key ) throws KeyParseException {
        CharSequence tokenValue = key.getSequence( TOKEN_SET );
        if ( tokenValue != null ) {
            Parameter next = null;

            if ( !key.atEnd() )
                next = parseParameter( key );

            if ( tokenValue.length() > 0 )
                return new Token( tokenValue.toString(), next );
            return next;
        } else {
            throw new KeyParseException( "Invalid key pattern: " + key.toString() );
        }
    }

    private static Parameter parseParameter( TokenIterator key ) throws KeyParseException {
        if ( key.getToken( '{' ) == null )
            throw new KeyParseException( "Expected '{' but got '" + key.getChars( 1 ) + "' after '" + key.viewCharsBefore() + "'" );

        CharSequence name = key.getSequence( TOKEN_SET, "}" );
        if ( name == null )
            throw new KeyParseException( "Expected '}' but got '" + key.getChars( 1 ) + "' after '" + key.viewCharsBefore() + "'" );

        if ( key.getToken( '}' ) == null )
            throw new KeyParseException( "Expected '{' but got '" + key.getChars( 1 ) + "' after '" + key.viewCharsBefore() + "'" );

        Token token = null;
        if ( !key.atEnd() ) {
            Processor next = parseToken( key );
            if ( next instanceof Token )
                token = (Token) next;
            else if ( next != null )
                throw new KeyParseException( "Parameters must have at least one character between it and the next parameter: '" + key.viewCharsBefore() + "'" );
        }

        return new Parameter( name.toString(), token );
    }

}
