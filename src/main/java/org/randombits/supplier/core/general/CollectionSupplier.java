/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.core.general;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import java.util.*;

/**
 * Supplies information about collections. Also supports any object which
 * can be converted to a Collection via the
 * {@link org.randombits.support.core.convert.ConversionAssistant}, which by default
 * includes {@link Iterable}, {@link Iterator}, {@link java.util.Enumeration} and
 * array values.
 *
 * @author David Peterson
 */
@SupplierPrefix("collection")
@SupportedTypes(Collection.class)
public class CollectionSupplier extends AnnotatedSupplier {

    /**
     * Constructs a Collection Supplier
     */
    public CollectionSupplier() {
    }

    @SupplierKey({"size", "length"})
    @KeyWeight(100)
    @API("1.0.0")
    public Integer getSize(@KeyValue Collection<?> collection) {
        return collection.size();
    }

    @SupplierKey("is empty")
    @API("1.0.0")
    public Boolean isEmpty(@KeyValue Collection<?> collection) {
        return collection.isEmpty();
    }

    /**
     * Joins the collection with the specified substring.
     *
     * @param collection The collection.
     * @param separator  The separator.
     * @return The joined string.
     */
    @SupplierKey("join with {separator}")
    @API("1.0.0")
    public String joinWith(@KeyValue Collection<?> collection, @KeyParam("separator") String separator) {
        if (isQuoted(separator))
            separator = separator.substring(1, separator.length() - 2);
        return StringUtils.join(collection.iterator(), separator);
    }

    @SupplierKey("random")
    @API("1.0.0")
    public Object getRandom(@KeyValue Collection<?> coll) {
        if (coll.size() == 0)
            return null;

        int index = (int) (coll.size() * Math.random());
        return coll.toArray()[index];
    }

    @SupplierKey("shuffle")
    @API("1.0.0")
    public Collection<?> shuffle(@KeyValue Collection<?> coll) {
        List<?> shuffledList = new java.util.ArrayList<Object>(coll);
        Collections.shuffle(shuffledList);
        return shuffledList;
    }

    @SupplierKey("reverse")
    @API("1.0.0")
    public Collection<?> reverse(@KeyValue Collection<?> coll) {
        List<?> reversedList = new java.util.ArrayList<Object>(coll);
        Collections.reverse(reversedList);
        return reversedList;
    }

    @SupplierKey("first")
    @API("1.0.0")
    public Object getFirst(@KeyValue Collection<?> coll) {
        Iterator<?> i = coll.iterator();
        if (i.hasNext())
            return i.next();
        return null;
    }

    @SupplierKey("last")
    @API("1.0.0")
    public Object getLast(@KeyValue Collection<?> coll) {
        // Shortcut for List instances.
        if (coll instanceof List<?>) {
            List<?> list = (List<?>) coll;
            return list.get(list.size() - 1);
        }

        // Otherwise, loop it.
        Object value = null;
        for (Object item : coll)
            value = item;
        return value;
    }

    @SupplierKey({"get {index}", "{index}"})
    @KeyWeight(-100)
    @API("1.0.0")
    public Object getIndex(@KeyValue Collection<?> coll, @KeyParam("index") String indexString) {
        int index = NumberUtils.toInt(indexString, 0);

        if (index > 0 && index <= coll.size()) {
            if (coll instanceof List<?>) { // get it directly
                return ((List<?>) coll).get(index - 1);
            } else { // loop through the iterator.
                int count = 1;
                for (Object value : coll) {
                    if (count == index)
                        return value;
                    count++;
                }
            }
        }

        return null;
    }

    @SupplierKey("collect {keychain}")
    @API("1.0.0")
    public Collection<?> collect(@KeyValue Collection<?> coll, @KeyParam("keychain") String keychain, @KeyContext SupplierContext context) throws CloneNotSupportedException, SupplierException {
        List<Object> values = new ArrayList<Object>(coll.size());
        for (Object value : coll) {
            values.add(context.getSupplierAssistant().findValue(context.clone().withValue(value), keychain));
        }
        return values;
    }

    private boolean isQuoted(String str) {
        return str.length() > 1
                && (str.charAt(0) == '"' && str.charAt(str.length() - 1) == '"' || str.charAt(0) == '\''
                && str.charAt(str.length() - 1) == '\'');
    }
}
