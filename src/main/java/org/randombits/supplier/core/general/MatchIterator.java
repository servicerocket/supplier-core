package org.randombits.supplier.core.general;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;

/**
 * A simple iterator which loops through the 'find' results of the specified
 * matcher.
 */
public class MatchIterator implements Iterator<Match> {

    private final Matcher matcher;

    boolean requireFind = true;

    boolean hasNext = false;
    
    int index = 1;

    /**
     * Constructs a new find iterator based on the matcher, resetting it.
     * 
     * @param matcher
     *            The matcher.
     */
    public MatchIterator( Matcher matcher ) {
        this( matcher, true );
    }

    /**
     * Constructs a new find iterator based on the matcher. If specified, the
     * matcher will be reset before any iterations take place.
     * 
     * @param matcher
     *            The matcher.
     * @param reset
     *            If true
     */
    public MatchIterator( Matcher matcher, boolean reset ) {
        this.matcher = matcher;
        if ( reset )
            matcher.reset();
    }

    /**
     * Checks if there is a 'next' value by calling {@link java.util.regex.Matcher#find()}.
     */
    public boolean hasNext() {
        if ( requireFind ) {
            hasNext = matcher.find();
            requireFind = false;
        }
        return hasNext;
    }

    /**
     * Returns the matcher in its current state.
     */
    public Match next() {
        if ( hasNext() ) {
            requireFind = true;
            return new Match(matcher, index++);
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * @throws UnsupportedOperationException
     *             when called.
     */
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
