package org.randombits.supplier.core.general;

import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import java.util.Locale;
import java.util.TimeZone;

/**
 * Provides details for time zone instances.
 */
@SupplierPrefix("timezone")
@SupportedTypes(TimeZone.class)
@API("1.0.0")
public class TimeZoneSupplier extends AnnotatedSupplier {
    @SupplierKey("name")
    @API("1.0.0")
    public String getDisplayName(@KeyValue TimeZone tz, @KeyContext SupplierContext context) {
        Locale userLocale = context.getEnvironmentValue(Locale.class);
        if (userLocale == null)
            return tz.getDisplayName();
        else
            return tz.getDisplayName(userLocale);
    }

    @SupplierKey("use daylight time")
    @API("1.0.0")
    public boolean usesDaylightTime(@KeyValue TimeZone tz) {
        return tz.useDaylightTime();
    }

    @SupplierKey("id")
    @API("1.0.0")
    public String getId(@KeyValue TimeZone tz) {
        return tz.getID();
    }

    @SupplierKey("utc offset")
    @API("1.0.0")
    public int getUTCOffset(@KeyValue TimeZone tz) {
        return tz.getRawOffset();
    }


}
