/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.core.general;

import org.joda.time.ReadableDateTime;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.utils.date.DateUtils;
import org.randombits.utils.lang.API;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Supplies information about dates.
 *
 * @author David Peterson
 */
@SupplierPrefix("date")
@SupportedTypes({ReadableDateTime.class, Date.class})
public class DateSupplier extends AnnotatedSupplier {

    /**
     * Formats the current date value with the specified format. The format matches that of the standard SimpleDateFormat.
     *
     * @param context The current context.
     * @param format  The format.
     * @return The formatted text.
     * @throws org.randombits.supplier.core.SupplierException
     *          if there is a problem parsing the format.
     */
    @SupplierKey({"format {format}", "{format}"})
    @KeyWeight(-100)
    @API("1.0.0")
    public String format( @KeyContext SupplierContext context, @KeyParam("format") String format ) throws SupplierException {
        try {
            return formatDate( context, context.getValueAs( Date.class ), format );
        } catch ( IllegalArgumentException e ) {
            throw new SupplierException( "Unsupported format: " + format );
        }
    }

    @SupplierKey("round to {period}")
    @KeyWeight(5)
    @API("1.0.0")
    public ReadableDateTime roundTo( @KeyValue ReadableDateTime dateTime, @KeyParam("period") String period ) {
        return DateUtils.roundDate( dateTime, period );
    }

    @SupplierKey("shift {amount}")
    @KeyWeight(5)
    @API("1.0.0")
    public ReadableDateTime shift( @KeyValue ReadableDateTime dateTime, @KeyParam("amount") String amount ) {
        return DateUtils.shiftDate( dateTime, amount );
    }

    @KeyWeight(10)
    @SupplierKey("at midnight")
    @API("1.0.0")
    public ReadableDateTime toDateMidnight( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.toDateTime().toDateMidnight();
    }

    /**
     * Returns milliseconds since the epoch (01-Jan-1970 00:00:00 GMT).
     *
     * @param dateTime The ReadableDateTime being handled.
     * @return The instant in milliseconds.
     */
    @SupplierKey("milliseconds")
    @KeyWeight(15)
    @API("1.0.0")
    public Long getMilliseconds( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getMillis();
    }

    @SupplierKey("milli of second")
    @API("1.0.0")
    public Integer getMilliOfSecond( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getMillisOfSecond();
    }

    @SupplierKey("second of minute")
    @API("1.0.0")
    public Integer getSecondOfMinute( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getSecondOfMinute();
    }

    @SupplierKey("minute of hour")
    @API("1.0.0")
    public Integer getMinuteOfHour( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getMinuteOfHour();
    }

    @SupplierKey("hour of day")
    @API("1.0.0")
    public Integer getHourOfDay( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getHourOfDay();
    }

    @SupplierKey("day of year")
    @API("1.0.0")
    public Integer getDayOfYear( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getDayOfYear();
    }

    @SupplierKey("day of week")
    @API("1.0.0")
    public Integer getDayOfWeek( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getDayOfWeek();
    }

    @SupplierKey("day of month")
    @API("1.0.0")
    public Integer getDayOfMonth( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getDayOfMonth();
    }

    @SupplierKey("month of year")
    @API("1.0.0")
    public Integer getMonthOfYear( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getMonthOfYear();
    }

    @SupplierKey("year")
    @API("1.0.0")
    public Integer getYear( @KeyValue ReadableDateTime dateTime ) {
        return dateTime.getYear();
    }

    /**
     * Parses the specified string value according to the provided 'simple' date
     * pattern.
     *
     * @param context The supplier context.
     * @param value   The date value.
     * @param pattern The pattern.
     * @return the Date instance.
     * @throws IllegalArgumentException if the pattern is not valid.
     * @throws java.text.ParseException if there is a problem while parsing.
     */
    public static Date parseDate( SupplierContext context, String value, String pattern ) throws ParseException, IllegalArgumentException {
        // TODO: These methods are a temporary workaround until a more general
        // solution is built.
        if ( value == null )
            return null;

        DateFormat format = getDateFormat( context, pattern );
        return format.parse( value );
    }

    /**
     * Formats the date into a string matching the specified pattern.
     *
     * @param context The supplier context.
     * @param date    The date value.
     * @param pattern The date pattern.
     * @return the formatted date.
     * @throws IllegalArgumentException if the pattern is invalid.
     */
    public static String formatDate( SupplierContext context, Date date, String pattern ) {
        // TODO: These methods are a temporary workaround until a more general
        // solution is built.
        if ( date == null )
            return null;
        DateFormat format = getDateFormat( context, pattern );
        return format.format( date );
    }

    /**
     * Returns a {@link java.text.DateFormat} based on the pattern, which has been set to the current user's
     * selected time zone.
     *
     * @param context The supplier context.
     * @param pattern The date format pattern.
     * @return The DateFormat instance.
     * @throws IllegalArgumentException if there is a problem parsing the pattern.
     */
    public static DateFormat getDateFormat( SupplierContext context, String pattern ) {
        TimeZone timeZone = context.getEnvironmentValue( TimeZone.class );
        Locale locale = context.getEnvironmentValue( Locale.class );

        return getDateFormat( pattern, timeZone, locale );
    }

    /**
     * Returns a {@link java.text.DateFormat} based on the pattern, which has been set to the current user's
     * selected time zone.
     *
     * @param environmentAssistant The environment assistant.
     * @param pattern              The date format pattern.
     * @return The DateFormat instance.
     * @throws IllegalArgumentException if there is a problem parsing the pattern.
     */
    public static DateFormat getDateFormat( EnvironmentAssistant environmentAssistant, String pattern ) {
        TimeZone timeZone = environmentAssistant.getValue( TimeZone.class );
        Locale locale = environmentAssistant.getValue( Locale.class );

        return getDateFormat( pattern, timeZone, locale );
    }

    /**
     * Returns a {@link DateFormat} based on the specified pattern, using the provided time zone and locale.
     *
     * @param pattern  The pattern.
     * @param timeZone The time zone
     * @param locale   The locale
     * @return the new {@link DateFormat}.
     */
    private static DateFormat getDateFormat( String pattern, TimeZone timeZone, Locale locale ) {
        DateFormat format = new SimpleDateFormat( pattern );
        format.setTimeZone( timeZone );

        Calendar calendar = Calendar.getInstance();
        calendar.setMinimalDaysInFirstWeek(4);
        format.setCalendar(calendar);

        NumberFormat numberFormat = locale == null ? NumberFormat.getInstance() : NumberFormat.getInstance( locale );
        format.setNumberFormat( numberFormat );
        return format;
    }
}
