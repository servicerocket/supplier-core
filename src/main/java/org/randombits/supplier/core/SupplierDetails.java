package org.randombits.supplier.core;

import com.atlassian.plugin.Plugin;
import org.randombits.utils.lang.API;

/**
 * Provides information about a given supplier, such as what
 * plugin it is provided by and a human-friendly name, description, etc.
 */
@API(value = "4.0.0")
public interface SupplierDetails {

    @API( value = "4.0.0" )
    Supplier getSupplier();

    @API( value = "4.0.0" )
    String getSupplierClass();

    @API( value = "4.0.0" )
    Plugin getPlugin();

    @API( value = "4.0.0" )
    String getPluginKey();

    @API( value = "4.0.0" )
    String getModuleKey();

    @API( value = "4.0.0" )
    String getName();

    @API( value = "4.0.0" )
    String getDescription();
}
