package org.randombits.supplier.core;

import org.randombits.utils.lang.API;

/**
 * Provides the environmental context for executing a supplier keychain.
 */
@API("1.0.0")
public interface SupplierContext extends Cloneable {

    /**
     * @return The value that was originally passed into the supplier chain.
     */
    @API("1.0.0")
    Object getOriginalValue();

    /**
     * @return The current value being processed.
     */
    @API("1.0.0")
    Object getValue();

    /**
     * Attempts to return the current value as an instance of the provided
     * <code>type</code>. If it cannot be cast, or converted, <code>null</code> is returned.
     *
     * @param type The class to cast to.
     * @param <T>  The type.
     * @return The current value, as the specified type, or <code>null</code> if it could not be cast or converted.
     */
    @API("1.0.0")
    <T> T getValueAs( Class<T> type );

    /**
     * Checks if the current value can be retrieved as the provided type.
     *
     * @param type The type to check.
     * @return <code>true</code> if the value can be returned as the specified type.
     */
    @API("1.0.0")
    boolean canGetValueAs( Class<?> type );

    /**
     * Sets the current value to the provided <code>newValue</code>.
     *
     * @param newValue The new value.
     */
    @API("1.0.0")
    void setValue( Object newValue );

    /**
     * Sets the current value to the provided object.
     *
     * @param newValue The new value.
     * @return The same instance of the SupplierContext, with the new value set as the current value.
     */
    @API("1.0.0")
    SupplierContext withValue( Object newValue );

    /**
     * Retrieves the current environment value of the specified type. By default,
     * this will be sourced via the {@link org.randombits.support.core.env.EnvironmentAssistant},
     * but values can be overridden via the {@link #setEnvironmentValue(Class, Object)} and
     * {@link #withEnvironmentValue(Class, Object)} methods.
     *
     * @param type The type to return.
     * @param <T>  The type value.
     * @return The current value.
     */
    @API("1.0.0")
    <T> T getEnvironmentValue( Class<T> type );

    /**
     * Sets the value of the specified type to the new value.
     *
     * @param type  The type.
     * @param value The new value.
     * @param <T>   The type to set.
     */
    @API("1.0.0")
    <T> void setEnvironmentValue( Class<T> type, T value );

    @API("1.0.0")
    <T> SupplierContext withEnvironmentValue( Class<T> type, T value );

    /**
     * Retrieves a named value of a specific type from the context. Attributes can be used to pass information in
     * to a Supplier from outside. Because they are accessible from any Supplier, keys should be unique to the
     * target. It is recommended to use a package-style naming convention to avoid overlaps.
     *
     * @param name The name of the attribute.
     * @param type The type that is expected. If there is a value but it is not of the specified type,
     *             <code>null</code> is returned.
     * @param <T>  The type.
     * @return The named attribute value, or <code>null</code>.
     */
    @API("1.0.0")
    <T> T getAttribute( String name, Class<T> type );

    /**
     * Sets a named attribute value. See {@link #getAttribute(String, Class)}
     *
     * @param name  The name of the value.
     * @param value The new value?
     */
    @API("1.0.0")
    void setAttribute( String name, Object value );

    /**
     * Stores the named attribute value and returns the current SupplierContext.
     *
     * @param name  The name fo the attribute.
     * @param value The value of the attribute.
     * @return The same SupplierContext, with the attribute added.
     */
    @API("1.0.0")
    SupplierContext withAttribute( String name, Object value );

    /**
     * Returns the SupplierAssistant this context belongs to.
     *
     * @return The {@link SupplierAssistant}.
     */
    @API("1.0.0")
    SupplierAssistant getSupplierAssistant();

    /**
     * Performs a deep clone of the current SupplierContext. Note that
     * it does not support throwing {@link CloneNotSupportedException}.
     *
     * @return the clone of this context.
     */
    @SuppressWarnings("CloneDoesntDeclareCloneNotSupportedException")
    @API("1.0.0")
    SupplierContext clone() throws CloneNotSupportedException;
}
