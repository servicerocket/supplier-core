package org.randombits.supplier.core.param;

import org.randombits.utils.lang.ClassUtils;

/**
 * Abstract base class for {@link Adaptor} implementations.
 */
public abstract class AbstractAdaptor<T> implements Adaptor<T> {

    private final Class<T> targetType;

    @SuppressWarnings("unchecked")
    public AbstractAdaptor() {
        targetType = (Class<T>) ClassUtils.getTypeArguments( AbstractAdaptor.class, getClass() ).get( 0 );
    }

    public AbstractAdaptor( Class<T> targetType ) {
        this.targetType = targetType;
    }

    @Override
    public Class<T> getTargetType() {
        return targetType;
    }
}
