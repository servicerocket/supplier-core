/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.core;

import java.util.Collection;

/**
 * Suppliers that wish to provide 'descendants' should implement this interface.
 *
 * @author David Peterson
 */
public interface HierarchicalSupplier extends Supplier {

    final static Property<Object> PARENT = new Property<Object>() {
        @Override
        public Object get( Supplier supplier, SupplierContext context ) throws SupplierException {
            if ( supplier instanceof HierarchicalSupplier )
                return ( (HierarchicalSupplier) supplier ).getParent( context );
            return null;
        }
    };

    final static Property<Collection<?>> CHILDREN = new Property<Collection<?>>() {

        @Override
        public Collection<?> get( Supplier supplier, SupplierContext context ) throws SupplierException {
            if ( supplier instanceof HierarchicalSupplier )
                return ( (HierarchicalSupplier) supplier ).getChildren( context );
            return null;
        }
    };

    /**
     * Returns the context object's parent, or <code>null</code>.
     *
     * @param context The context object.
     * @return the context's parent object.
     * @throws SupplierException         if there is a problem while retrieving the parent.
     */
    public Object getParent( SupplierContext context ) throws SupplierException;

    /**
     * Returns the collection of children belonging to this object, or <code>null</code> if it has none.
     *
     * @param context The context object.
     * @return the collection of children.
     * @throws SupplierException         there is a problem while retrieving the children.
     */
    public Collection<?> getChildren( SupplierContext context ) throws SupplierException;
}
