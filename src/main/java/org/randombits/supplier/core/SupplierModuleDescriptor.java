package org.randombits.supplier.core;

import com.atlassian.plugin.ModuleDescriptor;

/**
 * A module descriptor for providing Supplier implementations.
 */
public interface SupplierModuleDescriptor extends ModuleDescriptor<Supplier> {
}
