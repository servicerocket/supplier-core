package org.randombits.supplier.core.special;

import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import java.util.Locale;
import java.util.TimeZone;

/**
 * Provides access to the supplier context.
 */
@SupplierPrefix(value = "context", required = true)
public class ContextSupplier extends AnnotatedSupplier {

    @SupplierKey({"tz", "time zone"})
    @API("1.0.0")
    public TimeZone getTimeZone(@KeyContext SupplierContext context) {
        return context.getEnvironmentValue(TimeZone.class);
    }

    @SupplierKey("set time zone to {time zone id}")
    @API("1.0.0")
    public TimeZone setTimeZoneTo(@KeyContext SupplierContext context, @KeyParam("time zone id") String timeZoneId) {
        TimeZone zone = TimeZone.getTimeZone(timeZoneId);
        context.withEnvironmentValue(TimeZone.class, zone);
        return zone;
    }

    @SupplierKey("locale")
    @API("1.0.0")
    public Locale getLocale(@KeyContext SupplierContext context) {
        return context.getEnvironmentValue(Locale.class);
    }

    @SupplierKey({
            "set locale to {language}",
            "set locale to {language}, {country}",
            "set locale to {language}, {country}, {variant}"
    })
    @API("1.0.0")
    public Locale setLocaleTo(@KeyContext SupplierContext context, @KeyParam("language") String lang,
                               @KeyParam("country") String country, @KeyParam("variant") String variant) {
        Locale newLocale = new Locale(lang, country, variant);
        context.setEnvironmentValue(Locale.class, newLocale);
        return newLocale;
    }

    @SupplierKey("value")
    @API("1.0.0")
    public Object getValue(@KeyContext SupplierContext context) {
        return context.getValue();
    }

    @SupplierKey("original value")
    @API("1.0.0")
    public Object getOriginalValue(@KeyContext SupplierContext context) {
        return context.getOriginalValue();
    }

    @SupplierKey("store {name}")
    @API("1.0.0")
    public Object storeValue(@KeyContext SupplierContext context, @KeyParam("name") String name) {
        context.setAttribute(name, context.getValue());
        return context.getValue();
    }

    @SupplierKey("retrieve {name}")
    @API("1.0.0")
    public Object retrieveValue(@KeyContext SupplierContext context, @KeyParam("name") String name) {
        return context.getAttribute(name, Object.class);
    }

    @SupplierKey("clear {name}")
    @API("1.0.0")
    public Object clearValue(@KeyContext SupplierContext context, @KeyParam("name") String name) {
        context.setAttribute(name, null);
        return context.getValue();
    }
}
