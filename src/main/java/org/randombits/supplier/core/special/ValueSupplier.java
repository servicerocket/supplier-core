package org.randombits.supplier.core.special;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import org.apache.commons.lang.StringUtils;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.supplier.core.general.DateSupplier;
import org.randombits.supplier.core.general.NumberSupplier;
import org.randombits.utils.lang.API;

import java.text.ParseException;
import java.util.Date;

@SupplierPrefix(value = "value", required = true)
@API("1.0.0")
public class ValueSupplier extends AnnotatedSupplier {

    private final UserManager userManager;

    public ValueSupplier(UserManager userManager) {
        this.userManager = userManager;
    }

    @SupplierKey("text {value}")
    @API("1.0.0")
    public String asText(@KeyParam("value") String value) {
        return value;
    }

    @SupplierKey("user-profile {value}")
    @API("1.0.0")
    public UserProfile asUserProfile(@KeyParam("value") String value) {
        return userManager.getUserProfile(value);
    }

    @SupplierKey("date({format}) {value}")
    @API("1.0.0")
    public Date asDate(@KeyContext SupplierContext context, @KeyParam("value") String value, @KeyParam("format") String params) throws SupplierException {
        if (StringUtils.isBlank(params))
            throw new SupplierException("Please provide a date format for the 'value' supplier.");

        try {
            return DateSupplier.parseDate(context, value, params);
        } catch (ParseException e) {
            throw new SupplierException("Unable to parse '" + value
                    + "' as a date with the specified format of '" + params + "'");
        } catch (IllegalArgumentException e) {
            throw new SupplierException("Unsupported date format: " + params);
        }
    }

    @SupplierKey({"number {value}", "number({format}) {value}"})
    @API("1.0.0")
    public Number asNumber(@KeyParam("value") String value, @KeyParam("format") String params) throws SupplierException {
        try {
            return NumberSupplier.parseNumber(value, params);
        } catch (ParseException e) {
            throw new SupplierException("Unable to parse '" + value
                    + "' as a date with the specified format of '" + params + "'");
        }
    }
}
